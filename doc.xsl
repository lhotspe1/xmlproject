<xsl:stylesheet version="1.0"
      xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
      xmlns:fo="http://www.w3.org/1999/XSL/Format">
  <xsl:output method="xml" indent="yes"/>
  <xsl:template match="state">
    <fo:root>
      <fo:layout-master-set>
        <fo:simple-page-master master-name="A4-portrait"
              page-height="29.7cm" page-width="21.0cm" margin="2cm">
          <fo:region-body/>
        </fo:simple-page-master>
      </fo:layout-master-set>
      <fo:page-sequence master-reference="A4-portrait">
        <fo:flow flow-name="xsl-region-body">
          <fo:block margin-bottom="10px">
            <xsl:apply-templates select="government/names" />
          </fo:block>
          <fo:block>
            <xsl:apply-templates select="introduction" />
          </fo:block>
        </fo:flow>
      </fo:page-sequence>
    </fo:root>
  </xsl:template>
  <xsl:template match="government/names">
	<fo:block font-size="20px">
		<xsl:value-of select="conventionalLong"/>
	</fo:block>
	<fo:block font-size="16px">
		<xsl:value-of select="conventionalShort"/>
	</fo:block>
  </xsl:template>
  <xsl:template match="introduction">
	<fo:block font-size="12px">
		<xsl:value-of select="background"/>
	</fo:block>
  </xsl:template>
</xsl:stylesheet>
