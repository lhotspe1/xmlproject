<?xml version="1.0" encoding="UTF-8"?>
<state>
   <introduction>
      <background>At the close of World War I, the Czechs and Slovaks of the former Austro-Hungarian Empire merged to form Czechoslovakia. During the interwar years, having rejected a federal system, the new country's predominantly Czech leaders were frequently preoccupied with meeting the increasingly strident demands of other ethnic minorities within the republic, most notably the Slovaks, the Sudeten Germans, and the Ruthenians (Ukrainians). On the eve of World War II, Nazi Germany occupied the territory that today comprises Czechia, and Slovakia became an independent state allied with Germany. After the war, a reunited but truncated Czechoslovakia (less Ruthenia) fell within the Soviet sphere of influence. In 1968, an invasion by Warsaw Pact troops ended the efforts of the country's leaders to liberalize communist rule and create "socialism with a human face," ushering in a period of repression known as "normalization." The peaceful "Velvet Revolution" swept the Communist Party from power at the end of 1989 and inaugurated a return to democratic rule and a market economy. On 1 January 1993, the country underwent a nonviolent "velvet divorce" into its two national components, the Czech Republic and Slovakia. The Czech Republic joined NATO in 1999 and the European Union in 2004. The country added the short-form name Czechia in 2016, while continuing to use the full form name, Czech Republic.</background>
      <flag>czechia</flag>
   </introduction>
   <geography>
      <location>Central Europe, between Germany, Poland, Slovakia, and Austria</location>
      <coordinates>
         <latitude>
            <degrees>49</degrees>
            <minutes>45</minutes>
            <orientation>N</orientation>
         </latitude>
         <longitude>
            <degrees>15</degrees>
            <minutes>30</minutes>
            <orientation>E</orientation>
         </longitude>
      </coordinates>
      <area>
         <land unit="km">77247</land>
         <water unit="km">1620</water>
         <comparison>117</comparison>
      </area>
      <boundaries>
         <boundary>
            <stateArea>Austria</stateArea>
            <length unit="km">402</length>
         </boundary>
         <boundary>
            <stateArea>Germany</stateArea>
            <length unit="km">704</length>
         </boundary>
         <boundary>
            <stateArea>Poland</stateArea>
            <length unit="km">796</length>
         </boundary>
         <boundary>
            <stateArea>Slovakia</stateArea>
            <length unit="km">241</length>
         </boundary>
      </boundaries>
      <coastline unit="km">0</coastline>
      <climates>temperate, cool summmers, cold, cloudy, humid winters</climates>
      <terrain>Bohemia in the west consists of rolling plains, hills, and plateaus surrounded by low mountains; Moravia in the east consists of very hilly country</terrain>
      <elevation>
         <mean unit="m">433</mean>
         <lowest>
            <name>Labe River</name>
            <size unit="m">155</size>
         </lowest>
         <highest>
            <name>Snezka</name>
            <size unit="m">1602</size>
         </highest>
      </elevation>
      <naturalResources>
         <naturalResource>hard coal</naturalResource>
         <naturalResource>soft coal</naturalResource>
         <naturalResource>kaolin</naturalResource>
         <naturalResource>clay</naturalResource>
         <naturalResource>graphite</naturalResource>
         <naturalResource>timber</naturalResource>
         <naturalResource>arable land</naturalResource>
      </naturalResources>
      <landUses>
         <landUse>
            <name>agricultural land</name>
            <percentage year="2011">54.8</percentage>
         </landUse>
         <landUse>
            <name>forest</name>
            <percentage year="2011">34.4</percentage>
         </landUse>
         <landUse>
            <name>other</name>
            <percentage year="2011">10.8</percentage>
         </landUse>
      </landUses>
      <populationDistribution>a fairly even distribution throughout most of the country, but the northern and eastern regions tend to have larger urban concentrations</populationDistribution>
      <naturalHazards>flooding</naturalHazards>
   </geography>
   <people>
      <population>
         <count year="2018">10686269</count>
         <comparison>85</comparison>
      </population>
      <nationality>
         <noun>
            <singular>Czech</singular>
            <plural>Czechs</plural>
         </noun>
         <adjective>Czech</adjective>
      </nationality>
      <ethnicGroups year="2011">
         <ethnicGroup>
            <name>Czech</name>
            <percentage>64.3</percentage>
         </ethnicGroup>
         <ethnicGroup>
            <name>Moravian</name>
            <percentage>5</percentage>
         </ethnicGroup>
         <ethnicGroup>
            <name>Slovak</name>
            <percentage>1.4</percentage>
         </ethnicGroup>
         <ethnicGroup>
            <name>other</name>
            <percentage>1.8</percentage>
         </ethnicGroup>
         <ethnicGroup>
            <name>unspecified</name>
            <percentage>27.5</percentage>
         </ethnicGroup>
      </ethnicGroups>
      <languages year="2011">
         <language official="true">
            <name>Czech</name>
            <percentage>95.4</percentage>
         </language>
         <language official="false">
            <name>Slovak</name>
            <percentage>1.4</percentage>
         </language>
         <language official="false">
            <name>other</name>
            <percentage>3</percentage>
         </language>
      </languages>
      <religions year="2011">
         <religion>
            <name>Roman Catholic</name>
            <percentage>10.4</percentage>
         </religion>
         <religion>
            <name>Protestant</name>
            <percentage>1.1</percentage>
         </religion>
         <religion>
            <name>other and unspecified</name>
            <percentage>54</percentage>
         </religion>
         <religion>
            <name>none</name>
            <percentage>34.5</percentage>
         </religion>
      </religions>
      <ageGroups year="2018">
         <ageGroup>
            <min>0</min>
            <max>14</max>
            <male>843800</male>
            <female>790128</female>
            <percentage>15.21</percentage>
         </ageGroup>
         <ageGroup>
            <min>15</min>
            <max>24</max>
            <male>514728</male>
            <female>483546</female>
            <percentage>9.34</percentage>
         </ageGroup>
         <ageGroup>
            <min>25</min>
            <max>54</max>
            <male>2404724</male>
            <female>2275309</female>
            <percentage>43.79</percentage>
         </ageGroup>
         <ageGroup>
            <min>55</min>
            <max>64</max>
            <male>638130</male>
            <female>669959</female>
            <percentage>12.24</percentage>
         </ageGroup>
         <ageGroup>
            <min>65</min>
            <male>865455</male>
            <female>1209490</female>
            <percentage>19.42</percentage>
         </ageGroup>
      </ageGroups>
      <medianAge year="2018">
         <total>42.5</total>
         <male>41.2</male>
         <female>43.8</female>
         <comparison>29</comparison>
      </medianAge>
   </people>
   <government>
      <names>
         <conventionalLong>Czech Republic</conventionalLong>
         <conventionalShort>Czechia</conventionalShort>
         <localLong>Ceska republika</localLong>
         <localShort>Cesko</localShort>
         <url>czechia</url>
      </names>
      <type>parliamentary republic</type>
      <capital>
         <name>Prague</name>
         <latitude>
            <degrees>50</degrees>
            <minutes>05</minutes>
            <orientation>N</orientation>
         </latitude>
         <longitude>
            <degrees>14</degrees>
            <minutes>28</minutes>
            <orientation>E</orientation>
         </longitude>
         <time>UTC+1</time>
      </capital>
      <anthem>
         <name>Kde domov muj?</name>
         <lyrics>Josef Kajetan Tyl</lyrics>
         <music>Frantisek Skroup</music>
      </anthem>
   </government>
   <economy>
      <gdp year="2015" unit="billion">351.9</gdp>
      <gdp year="2016" unit="billion">360.5</gdp>
      <gdp year="2017" unit="billion">375.9</gdp>
      <budget year="2017">
         <revenues unit="billion">87.37</revenues>
         <expenditures unit="billion">83.92</expenditures>
      </budget>
      <export>machinery and transport equipment, raw materials, fuel, chemicals</export>
      <imports>machinery and transport equipment, raw materials and fuels, chemicals</imports>
   </economy>
</state>
