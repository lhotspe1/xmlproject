adresář ./ obsahuje všechny zdrojové soubory projektu a script makescript.sh
	makescript.sh vygeneruje všechny finální soubory do adresáře ./output/

adresář ./output/ obsahuje všechny finální soubory, zejména pak (X)HTML a PDF
	při spuštění makescript.sh se všechny tyto soubory automaticky přepíší

použitý software: saxon9, fop, xmllint, Visual Studio Code, GitLab a další

použité weby: Zvon.org (web, prezentace a PDF), w3schools.com/XML (příklady)