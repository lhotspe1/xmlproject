#!/bin/bash

rm -rf output;
mkdir output;

cp "styles.css" "output/styles.css";
cp "script.js" "output/script.js";
cp "czechia.gif" "output/czechia.gif";
cp "slovakia.gif" "output/slovakia.gif";
cp "united-kingdom.gif" "output/united-kingdom.gif";
cp "united-states.gif" "output/united-states.gif";

jing "page.rng" "czechia.xml";
jing "page.rng" "slovakia.xml";
jing "page.rng" "united-kingdom.xml";
jing "page.rng" "united-states.xml";

saxon9 "czechiadtd.xml" "page.xslt" > "output/czechia.html";
saxon9 "slovakiadtd.xml" "page.xslt" > "output/slovakia.html";
saxon9 "united-kingdomdtd.xml" "page.xslt" > "output/united-kingdom.html";
saxon9 "united-statesdtd.xml" "page.xslt" > "output/united-states.html";

saxon9 "index.xml" "index.xslt" > "output/index.html";

fop/fop -xml "czechia.xml" -xsl "doc.xsl" -pdf "output/czechia.pdf";
fop/fop -xml "slovakia.xml" -xsl "doc.xsl" -pdf "output/slovakia.pdf";
fop/fop -xml "united-kingdom.xml" -xsl "doc.xsl" -pdf "output/united-kingdom.pdf";
fop/fop -xml "united-states.xml" -xsl "doc.xsl" -pdf "output/united-states.pdf";
