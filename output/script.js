function getValuesFrom(id, float = false) {
    var output = [];
    $("#" + id + "C" + (float ? "p" : "l")).text().split(",").forEach(item => output.push(float ? parseFloat(item) : item.trim()));
    output.pop();
    return output;
}

function getBackgroundColors() {
    return [
        "rgba(31, 119, 180, 1)",
        "rgba(255, 127, 14, 1)",
        "rgba(44, 160, 44, 1)",
        "rgba(214, 39, 40, 1)",
        "rgba(148, 103, 189, 1)",
        "rgba(140, 86, 75, 1)",
        "rgba(227, 119, 194, 1)",
        "rgba(127, 127, 127, 1)",
        "rgba(188, 189, 34, 1)",
        "rgba(23, 190, 207, 1)",
        "rgba(0, 0, 0, 1)",
        "rgba(255, 192, 0, 1)"
    ];
}

new Chart(document.getElementById("landUseChart").getContext('2d'), {
    type: 'bar',
    data: {
        labels: getValuesFrom("lU"),
        datasets: [{
            data: getValuesFrom("lU", true),
            backgroundColor: getBackgroundColors()
        }]
    },
    options: {
        legend: {
            display: false
        },
        title: {
            display: false
        }
    }
});

new Chart(document.getElementById("ethnicGroupsChart").getContext('2d'), {
    type: 'doughnut',
    data: {
        labels: getValuesFrom("eG"),
        datasets: [{
            data: getValuesFrom("eG", true),
            backgroundColor: getBackgroundColors()
        }]
    },
    options: {
        legend: {
            display: true
        },
        title: {
            display: false
        }
    }
});

new Chart(document.getElementById("languagesChart").getContext('2d'), {
    type: 'doughnut',
    data: {
        labels: getValuesFrom("l"),
        datasets: [{
            data: getValuesFrom("l", true),
            backgroundColor: getBackgroundColors()
        }]
    },
    options: {
        legend: {
            display: true
        },
        title: {
            display: false
        }
    }
});

new Chart(document.getElementById("religionsChart").getContext('2d'), {
    type: 'doughnut',
    data: {
        labels: getValuesFrom("r"),
        datasets: [{
            data: getValuesFrom("r", true),
            backgroundColor: getBackgroundColors()
        }]
    },
    options: {
        legend: {
            display: true
        },
        title: {
            display: false
        }
    }
});

new Chart(document.getElementById("gdpChart").getContext('2d'), {
    type: 'line',
    data: {
        labels: getValuesFrom("g"),
        datasets: [{
            data: getValuesFrom("g", true)
        }]
    },
    options: {
        legend: {
            display: false
        },
        title: {
            display: false
        }
    }
});
