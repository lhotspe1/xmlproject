<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">


<xsl:output method="html" encoding="utf-8" indent="yes" />


<xsl:template match="state">
<xsl:text disable-output-escaping='yes'>&lt;!DOCTYPE html&gt;</xsl:text>
<html lang="en">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <title>XML Project - Lhotský</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous" />
        <link rel="stylesheet" href="styles.css" />
    </head>
    <body>
        <xsl:apply-templates select="government" mode="top" />
        <xsl:apply-templates select="introduction" />
        <xsl:apply-templates select="geography" />
        <xsl:apply-templates select="people" />
        <xsl:apply-templates select="government" />
        <xsl:apply-templates select="economy" />
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
        <script src="https://kit.fontawesome.com/61c2400f65.js" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.min.js"></script>
        <script src="script.js"></script>
    </body>
</html>
</xsl:template>


<xsl:template match="government" mode="top">
        <div class="container mt-3 mb-4">
            <div class="row">
                <div class="col">
		    <h1><xsl:value-of select="names/conventionalLong" /></h1>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <h2><xsl:value-of select="names/conventionalShort" /></h2>
                </div>
                <div class="col-md-8 text-md-right">
                    <h2><xsl:value-of select="type" /></h2>
                </div>
            </div>
        </div>
</xsl:template>


<xsl:template match="introduction">
        <div class="container shadow-sm p-3 rounded mb-4">
            <h2><i class="far fa-file-alt"></i> Introduction</h2>
            <div class="row">
                <div class="col-md-8">
                    <p><xsl:value-of select="background" /></p>
                </div>
                <div class="col-md-4">
                    <img src="{flag}.gif" alt="{flag}" />
                </div>
            </div>
        </div>
</xsl:template>


<xsl:template match="geography">
        <div class="container shadow-sm p-3 rounded mb-4">
            <div class="row">
                <div class="col">
                    <h2><i class="fas fa-search-location"></i> Geography</h2>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <p><xsl:value-of select="location" /></p>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <h4>Information:</h4>
                </div>
            </div>
            <div class="row">
                <xsl:apply-templates select="coordinates" />
                <xsl:apply-templates select="area" />
            </div>
            <div class="row">
                <div class="col">
                    <h4>Boundaries:</h4>
                </div>
            </div>
            <div class="row">
                <xsl:apply-templates select="boundaries/boundary" />
            </div>
            <div class="row">
                <div class="col">
                    <h4>Terrain:</h4>
                    <p><xsl:value-of select="terrain" /></p>
                    <h4>Coastline:</h4>
                    <p><xsl:value-of select="coastline" /> <xsl:value-of select="coastline/@unit" /></p>
                    <h4>Climates:</h4>
                    <p><xsl:value-of select="climates" /></p>
                    <h4>Elevation:</h4>
                </div>
            </div>
            <div class="row">
                <xsl:apply-templates select="elevation" />
            </div>
            <div class="row">
                <div class="col-md-7">
                    <h4>Natural Resources:</h4>
                    <div class="col-md-12 container">
                        <ul class="list-unstyled row">
                            <xsl:apply-templates select="naturalResources/naturalResource" />
                        </ul>
                    </div>
                </div>
                <div class="col-md-5">
                    <h4>Land Use:</h4>
                    <canvas height="100" id="landUseChart">
                        <span id="lUCp">
                            <xsl:apply-templates select="landUses/landUse" mode="p" />
                        </span>
                        <span id="lUCl">
                            <xsl:apply-templates select="landUses/landUse" mode="l" />
                        </span>
                    </canvas>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <h4>Population Distribution:</h4>
                    <p><xsl:value-of select="populationDistribution" /></p>
                    <h4>Natural Hazard:</h4>
                    <p><xsl:value-of select="naturalHazards" /></p>
                </div>
            </div>
        </div>
</xsl:template>


<xsl:template match="people">
        <div class="container shadow-sm p-3 rounded mb-4">
            <div class="row">
                <div class="col">
                    <h2><i class="fas fa-user-friends"></i> People</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md">
                    <span>count (<xsl:value-of select="population/count/@year" />): <xsl:value-of select="population/count" /></span>
                </div>
                <xsl:apply-templates select="nationality" />
            </div>
            <div class="row">
                <div class="col">
                    <h4>Ethnic Groups (<xsl:value-of select="ethnicGroups/@year" />):</h4>
                    <canvas height="200" id="ethnicGroupsChart">
                        <span id="eGCp">
                            <xsl:apply-templates select="ethnicGroups/ethnicGroup" mode="p" />
                        </span>
                        <span id="eGCl">
                            <xsl:apply-templates select="ethnicGroups/ethnicGroup" mode="l" />
                        </span>
                    </canvas>
                </div>
                <div class="col">
                    <h4>Languages (<xsl:value-of select="languages/@year" />):</h4>
                    <canvas height="200" id="languagesChart">
                        <span id="lCp">
                            <xsl:apply-templates select="languages/language" mode="p" />
                        </span>
                        <span id="lCl">
                            <xsl:apply-templates select="languages/language" mode="l" />
                        </span>
                    </canvas>
                </div>
                <div class="col">
                    <h4>Religions (<xsl:value-of select="religions/@year" />):</h4>
                    <canvas height="200" id="religionsChart" class="margin-temp">
                        <span id="rCp">
                            <xsl:apply-templates select="religions/religion" mode="p" />
                        </span>
                        <span id="rCl">
                            <xsl:apply-templates select="religions/religion" mode="l" />
                        </span>
                    </canvas>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <h4>Age Groups (<xsl:value-of select="ageGroups/@year" />):</h4>
                    <table class="table table-borderless table-sm">
                        <thead>
                            <tr>
                                <th>Group</th>
                                <th>Male</th>
                                <th>Female</th>
                                <th>Percentage</th>
                            </tr>
                        </thead>
                        <tbody>
                            <xsl:apply-templates select="ageGroups/ageGroup" />
                        </tbody>
                    </table>
                    <h4>Median Age (<xsl:value-of select="medianAge/@year" />):</h4>
                </div>
            </div>
            <div class="row">
                <xsl:apply-templates select="medianAge" />
            </div>
        </div>
</xsl:template>


<xsl:template match="government">
        <div class="container shadow-sm p-3 rounded mb-4">
            <div class="row">
                <div class="col">
                    <h2><i class="far fa-building"></i> Government</h2>
                </div>
            </div>
            <div class="row">
                <xsl:apply-templates select="names" />
            </div>
            <div class="row">
                <div class="col">
                    <h4>Capital:</h4>
                </div>
            </div>
            <div class="row">
                <xsl:apply-templates select="capital" />
            </div>
            <div class="row">
                <div class="col">
                    <h4>Anthem:</h4>
                </div>
            </div>
            <div class="row">
                <xsl:apply-templates select="anthem" />
            </div>
        </div>
</xsl:template>


<xsl:template match="economy">
        <div class="container shadow-sm p-3 rounded mb-4">
            <div class="row">
                <div class="col">
                    <h2><i class="far fa-money-bill-alt"></i> Economy</h2>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <h4>Gross domestic product (<xsl:value-of select="gdp[1]/@unit" />):</h4>
                    <canvas height="100" id="gdpChart" class="margin-temp">
                        <span id="gCp">
                            <xsl:apply-templates select="gdp" mode="p" />
                        </span>
                        <span id="gCl">
                            <xsl:apply-templates select="gdp" mode="l" />
                        </span>
                    </canvas>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <h4>Budget:</h4>
                </div>
            </div>
            <div class="row">
                <xsl:apply-templates select="budget" />
            </div>
            <div class="row">
                <div class="col">
                    <h4>Export:</h4>
                    <p><xsl:value-of select="export" /></p>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <h4>Import:</h4>
                    <p><xsl:value-of select="imports" /></p>
                </div>
            </div>
        </div>
</xsl:template>


<xsl:template match="geography/coordinates">
    <div class="col-md">
        <span>latitude: <xsl:apply-templates select="latitude" /></span>
    </div>
    <div class="col-md">
        <span>longitude: <xsl:apply-templates select="longitude" /></span>
    </div>
</xsl:template>


<xsl:template match="latitude | longitude">
    <xsl:value-of select="degrees" />° <xsl:value-of select="minutes" />' <xsl:value-of select="orientation" />
</xsl:template>


<xsl:template match="geography/area">
    <div class="col-md">
        <span>land: <xsl:value-of select="land" /> <xsl:value-of select="land/@unit" /><sup>2</sup></span>
    </div>
    <div class="col-md">
        <span class="margin-temp">water: <xsl:value-of select="water" /> <xsl:value-of select="water/@unit" /><sup>2</sup></span>
    </div>
</xsl:template>


<xsl:template match="geography/boundaries/boundary">
    <div class="col-md">
        <xsl:choose>
            <xsl:when test ="position() = last()">
                <span class="margin-temp">
                    <xsl:value-of select="stateArea" />, <xsl:value-of select="length" /> <xsl:value-of select="length/@unit" />
                </span>
            </xsl:when>
            <xsl:otherwise>
                <span>
                    <xsl:value-of select="stateArea" />, <xsl:value-of select="length" /> <xsl:value-of select="length/@unit" />
                </span>
            </xsl:otherwise>
        </xsl:choose>
    </div>
</xsl:template>


<xsl:template match="geography/elevation">
    <div class="col-md">
        <span>Lowest: <xsl:apply-templates select="lowest" /></span>
    </div>
    <div class="col-md">
        <span>Mean: <xsl:value-of select="mean" /> <xsl:value-of select="mean/@unit" /></span>
    </div>
    <div class="col-md">
        <span class="margin-temp">Highest: <xsl:apply-templates select="highest" /></span>
    </div>
</xsl:template>


<xsl:template match="lowest | highest">
        <xsl:value-of select="name" />, <xsl:value-of select="size" /> <xsl:value-of select="size/@unit" />
</xsl:template>


<xsl:template match="geography/naturalResources/naturalResource">
    <li class="list-item col-4 py-2"><xsl:value-of select="." /></li>
</xsl:template>


<xsl:template match="geography/landUses/landUse" mode="p">
    <xsl:value-of select="percentage" />,
</xsl:template>


<xsl:template match="geography/landUses/landUse" mode="l">
    <xsl:value-of select="name" />,
</xsl:template>


<xsl:template match="people/nationality">
    <div class="col-md">
        <span>singular: <xsl:value-of select="noun/singular" /></span>
    </div>
    <div class="col-md">
        <span>plural: <xsl:value-of select="noun/plural" /></span>
    </div>
    <div class="col-md">
        <span class="margin-temp">adjective: <xsl:value-of select="adjective" /></span>
    </div>
</xsl:template>


<xsl:template match="people/ethnicGroups/ethnicGroup" mode="p">
    <xsl:value-of select="percentage" />,
</xsl:template>


<xsl:template match="people/ethnicGroups/ethnicGroup" mode="l">
    <xsl:value-of select="name" />,
</xsl:template>


<xsl:template match="people/languages/language" mode="p">
    <xsl:value-of select="percentage" />,
</xsl:template>


<xsl:template match="people/languages/language" mode="l">
    <xsl:value-of select="name" />,
</xsl:template>


<xsl:template match="people/religions/religion" mode="p">
    <xsl:value-of select="percentage" />,
</xsl:template>


<xsl:template match="people/religions/religion" mode="l">
    <xsl:value-of select="name" />,
</xsl:template>


<xsl:template match="people/ageGroups/ageGroup">
    <tr>
        <xsl:choose>
            <xsl:when test="max != ''">
                <td><xsl:value-of select="min" /> - <xsl:value-of select="max" /></td>
            </xsl:when>
            <xsl:otherwise>
                <td><xsl:value-of select="min" />+</td>
            </xsl:otherwise>
        </xsl:choose>
        <td><xsl:value-of select="male" /></td>
        <td><xsl:value-of select="female" /></td>
        <td><xsl:value-of select="percentage" /></td>
    </tr>
</xsl:template>


<xsl:template match="people/medianAge">
    <div class="col-md">
        <span>total: <xsl:value-of select="total" /></span>
    </div>
    <div class="col-md">
        <span>male: <xsl:value-of select="male" /></span>
    </div>
    <div class="col-md">
        <span>female: <xsl:value-of select="female" /></span>
    </div>
</xsl:template>


<xsl:template match="government/names">
    <xsl:choose>
        <xsl:when test="localLong != ''">
            <div class="col-md-6">
                <span>conventional long: <xsl:value-of select="conventionalLong" /></span>
            </div>
            <div class="col-md-6">
                <span>conventional short: <xsl:value-of select="conventionalShort" /></span>
            </div>
            <div class="col-md-6">
                <span>local long: <xsl:value-of select="localLong" /></span>
            </div>
            <div class="col-md-6">
                <span class="margin-temp">local short: <xsl:value-of select="localShort" /></span>
            </div>
        </xsl:when>
        <xsl:otherwise>
            <div class="col-md-6">
                <span>conventional long: <xsl:value-of select="conventionalLong" /></span>
            </div>
            <div class="col-md-6">
                <span class="margin-temp">conventional short: <xsl:value-of select="conventionalShort" /></span>
            </div>
        </xsl:otherwise>
    </xsl:choose>
</xsl:template>


<xsl:template match="government/capital">
    <div class="col-md">
        <span>name: <xsl:value-of select="name" /></span>
    </div>
    <div class="col-md">
        <span>latitude: <xsl:apply-templates select="latitude" /></span>
    </div>
    <div class="col-md">
        <span>longitude: <xsl:apply-templates select="longitude" /></span>
    </div>
    <div class="col-md">
        <span class="margin-temp">time: <xsl:value-of select="time" /></span>
    </div>
</xsl:template>


<xsl:template match="government/anthem">
    <div class="col-md">
        <span>name: <xsl:value-of select="name" /></span>
    </div>
    <div class="col-md">
        <span>lyrics: <xsl:value-of select="lyrics" /></span>
    </div>
    <div class="col-md">
         <span class="margin-temp">music: <xsl:value-of select="music" /></span>
    </div>
</xsl:template>


<xsl:template match="economy/gdp" mode="p">
    <xsl:value-of select="." />,
</xsl:template>


<xsl:template match="economy/gdp" mode="l">
    <xsl:value-of select="@year" />,
</xsl:template>


<xsl:template match="economy/budget">
    <div class="col-md">
        <span>revenues (<xsl:value-of select="revenues/@unit" />): <xsl:value-of select="revenues" /></span>
    </div>
    <div class="col-md">
        <span class="margin-temp">expenditures (<xsl:value-of select="expenditures/@unit" />): <xsl:value-of select="expenditures" /></span>
    </div>
</xsl:template>


</xsl:stylesheet>
