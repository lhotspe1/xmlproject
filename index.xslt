<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">


<xsl:output method="html" encoding="utf-8" indent="yes" />


<xsl:template match="states">
<xsl:text disable-output-escaping='yes'>&lt;!DOCTYPE html&gt;</xsl:text>
<html lang="en">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <title>XML Project - Lhotský</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous" />
        <link rel="stylesheet" href="styles.css" />
    </head>
    <body>
        <xsl:apply-templates select="s/state/government" mode="top" />
    </body>
</html>
</xsl:template>


<xsl:template match="s/state/government" mode="top">
        <div class="container mt-3 mb-4 p-2 shadow-sm">
            <div class="row">
                <div class="col">
		    <h4><xsl:value-of select="names/conventionalLong" /></h4>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <h5><xsl:value-of select="names/conventionalShort" /></h5>
                </div>
                <div class="col-md-8 text-md-right">
                    <h5><xsl:value-of select="type" /></h5>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <span><a href="{names/url}.html">more information</a></span>
                </div>
            </div>
        </div>
</xsl:template>


</xsl:stylesheet>
